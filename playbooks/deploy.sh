#!/bin/bash
ROOT="/usr/local/bm/event-loop-logs-batch"

chown -R bm $ROOT
chmod -R g+w $ROOT
cd $ROOT/playbooks

ansible-playbook -i "localhost," -c local playbooks/cron.yml
