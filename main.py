from datetime import datetime as dt
import os
import argparse
import itertools
import gzip
import time

from generators import *

parser = argparse.ArgumentParser(description='S3 Batch uploader')
parser.add_argument('--date', default=dt.now().strftime('%Y-%m-%d'),
                    help='Date - format YYYY-MM-DD')
parser.add_argument('--hour', default=dt.now().strftime('%H'),
                    help='Hour - format H')

LOG_FILE_PATH = os.getenv('LOG_FILE_PATH', '/tmp/event-loop/')
LOG_FILE_TMP_PATH = os.getenv('LOG_FILE_TMP_PATH', '/tmp/logs/')
DEST_PATH = os.getenv('DEST_PATH', '/tmp/s3/')


def fid_sort_key(line):
    return line.split('|')[0]


def dt_sort_key(line):
    return line.split('|')[1]


def get_dest_file_name(type, hour):
    return "{}{}.gz".format(type, hour)


def get_dest_file_path(date, fid):
    return "{}/{:%Y/%m/%d}/{}".format(DEST_PATH, date, fid)


def get_file_pattern(type, date, hour):
    return (r'{}.*.txt.{}_{}.*').format(type, date, hour)


def write_file(name, path, lines):
    if not os.path.exists(path):
        os.makedirs(path)

    filename = os.path.join(path, name)

    with gzip.open(filename, 'wt') as f:
        for line in lines:
            f.write(line)


def process_files(date, hour, type):
    # logs_path = os.path.join(LOG_FILE_PATH, date)
    logs_path = LOG_FILE_PATH
    file_pattern = get_file_pattern(type, date, hour)

    lognames = gen_find(file_pattern, logs_path)
    tmpnames = gen_bckp(lognames, LOG_FILE_TMP_PATH)

    sortednames = gen_sort(tmpnames, '|', '1,2')

    logfiles = gen_open(sortednames)
    loglines = gen_cat(logfiles)

    for fid, group in itertools.groupby(loglines, key=fid_sort_key):
        dst_filename = get_dest_file_name(type, hour)
        dst_filepath = get_dest_file_path(dt.strptime(date, '%Y-%m-%d'), fid)

        write_file(dst_filename, dst_filepath, group)

    lognames = gen_find(file_pattern, logs_path)
    to_delete_file = "{}/{}-{}.txt".format(DEST_PATH, date, type)

    with open(to_delete_file, 'w+') as file:
        for name in lognames:
            file.write("{}\n".format(name))


if __name__ == '__main__':
    args = parser.parse_args()
    date = args.date
    hour = args.hour

    events = ["win", "loss", "click"]

    if not os.path.exists(LOG_FILE_TMP_PATH):
        os.makedirs(LOG_FILE_TMP_PATH)

    if not os.path.exists(DEST_PATH):
        os.makedirs(DEST_PATH)

    start = time.time()

    for event in events:
        print(" [x] Processing {}'s in {} {}hs".format(event, date, hour))
        process_files(date, hour, event)

    end = time.time()
    print(end - start)
