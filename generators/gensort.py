import subprocess
import shlex
import shutil


def gen_sort(files, separator=',', keys='1'):
    sortCommand = "sort --field-separator='{}' --key={} {}"

    for logfile in files:
        command = sortCommand.format(separator, keys, logfile)
        with open(logfile, "r+") as out:
            subprocess.Popen(shlex.split(command), stdout=out)
        yield logfile
