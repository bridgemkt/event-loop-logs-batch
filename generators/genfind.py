# A function that generates files that match a given filename pattern

import os
import fnmatch
import re


def gen_find(pattern, top):
    for path, dirlist, filelist in os.walk(top):
        for name in filelist:
            if not re.match(pattern, name):
                continue
            yield os.path.join(path, name)

# def gen_find(filepat, top):
#     for path, dirlist, filelist in os.walk(top):
#         for name in fnmatch.filter(filelist, filepat):
#
#             yield os.path.join(path, name)


# Example use
if __name__ == '__main__':
    pattern = (r'{}.*{}.txt*').format("win", "17")
    for filename in gen_find(pattern, './files'):
        print(filename)
