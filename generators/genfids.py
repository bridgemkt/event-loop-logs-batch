def gen_fids(lines):
    seen = set()
    for line in lines:
        fid = line.split(',')[0]
        if fid not in seen:
            yield fid
            seen.add(fid)
