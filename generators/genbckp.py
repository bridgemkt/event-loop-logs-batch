import shutil
import os


def gen_bckp(files, dst_path):
    for file in files:
        yield shutil.copy(file, os.path.join(dst_path, file.split('/')[-1]))
