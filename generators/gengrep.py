# Grep a sequence of lines that match a re pattern

import re


def gen_grep(pat, lines):
    patc = re.compile(pat)
    for line in lines:
        if patc.search(line):
            yield line


# Example use
if __name__ == '__main__':
    from genfind import gen_find
    from genopen import gen_open
    from gencat import gen_cat

    lognames = gen_find("*.gz", "./files")
    logfiles = gen_open(lognames)
    loglines = gen_cat(logfiles)

    # Look for flight_id f1234
    fid_lines = gen_grep(r'f1234', loglines)
    for line in fid_lines:
        print(line)
