#!/bin/bash
PYTHON="/usr/bin/python3"
PROJECT_PATH="/usr/local/bm/event-loop-logs-batch"
AWS="/usr/bin/aws"
BUCKET="b8ba77c90bbca-eventloopdevlogsbucket"
DEBUG=1


# DEBUG info
if [ "$DEBUG" -eq 1 ];
then
  echo "env DEST_PATH: $DEST_PATH" >> /tmp/logs_processor.log
fi

#0: project configuration
TODAY=`date +'%Y-%m-%d'`
cd $PROJECT_PATH

#1: Generar logs con batch
echo "logs processing"
$PYTHON main.py --date $TODAY --hour `date +'%H' --date='1 hour ago'`

#2: iterate over used files and delete them
if [ "$?" -eq 0 ];
then
  echo "logs processing succesful, removing logs from EFS"
  recycle_click="$DEST_PATH/$TODAY-click.txt"
  recycle_win="$DEST_PATH/$TODAY-win.txt"
  recycle_loss="$DEST_PATH/$TODAY-loss.txt"
  recycle=`cat $recycle_click $recycle_win $recycle_loss`

  for file in $recycle
  do
    echo "rm: $file"
    rm $file
  done
else
  echo "logs processing unsuccesful, not removing files"
  echo "exiting"
  exit 200
fi

#3: syncronize to s3
cd $DEST_PATH
echo "syncronizing to s3://$BUCKET/"
$AWS s3 sync --exclude="*.txt" . s3://$BUCKET/
upload_status=$?

#4: remove local files
if [ "$?" -eq 0 ];
then
  echo "sync: OK"
  rm -rf $DEST_PATH/*
else
  echo "sync: PROBLEM. not deleting"
fi
